cube(`Products`, {
  sql: `SELECT * FROM products `,

  measures: {
    totalStock: {
      sql: 'quantityInStock',
      type: 'sum'
    }
  },

  dimensions: {
    productType: {
      sql: 'productLine',
      type: 'string'
    }
  }
});
